## Description

Ce projet est un jeu captivant dans lequel l'utilisateur est plongé dans un univers sombre et mystérieux. En naviguant à travers différents sites, qui deviennent de plus en plus obscurs, le joueur se lance dans une quête pour découvrir des indices et résoudre des énigmes. L'objectif ultime est de comprendre à qui appartient l'ordinateur mystérieux qu'il a découvert.

Le jeu se déroule dans le contexte du darknet, où les utilisateurs explorent des territoires virtuels clandestins et dangereux. En incarnant le personnage principal, vous vous embarquez dans une aventure palpitante où chaque site visité présente de nouveaux défis et secrets à dévoiler.

## Objectif du jeu

L'objectif principal de ce jeu est de permettre aux joueurs de relever des défis intellectuels en résolvant des énigmes et en collectant des indices. À mesure qu'ils progressent dans le jeu, les joueurs doivent naviguer de site en site, explorant des recoins sombres du cyberespace pour découvrir des informations cruciales.

Chaque site visité propose une énigme unique à résoudre. En répondant correctement à ces énigmes, les joueurs obtiennent des indices précieux qui les guideront vers le site suivant. Le niveau de difficulté des énigmes augmentera au fur et à mesure de leur progression, offrant ainsi un défi croissant et une immersion plus profonde dans l'univers du jeu.

## Instructions et fonctionnalités

1. Navigation entre les sites: Les joueurs peuvent naviguer entre les différents sites en utilisant une interface intuitive. Chaque site représente une étape cruciale de l'enquête et propose une expérience unique.

2. Résolution d'énigmes: Chaque site contient une énigme à résoudre pour obtenir un indice. Les énigmes peuvent varier en termes de difficulté et de nature, allant des casse-têtes logiques aux défis de codage ou de déchiffrement.

3. Collecte d'indices: Les joueurs doivent trouver et collecter des indices tout au long de leur exploration. Ces indices les aideront à reconstituer le puzzle de l'histoire et à comprendre qui est le mystérieux propriétaire de l'ordinateur découvert.

4. Progression et narration: Le jeu est conçu de manière à ce que la progression de l'utilisateur soit liée à la narration. À mesure qu'ils avancent dans l'enquête, les joueurs découvrent de nouvelles informations, des personnages intrigants et des rebondissements inattendus.

5. Interface immersive: L'interface du jeu est soigneusement conçue pour offrir une expérience immersive. Des visuels captivants, des effets sonores et une musique adaptée contribuent à plonger les joueurs dans l'atmosphère sombre et mystérieuse du jeu.


## Comment jouer

1. Lancez le jeu en ouvrant le fichier "index.html" dans votre navigateur.

2. Une fois dans le jeu, utilisez les commandes de navigation pour passer d'un site à un autre. Explorez chaque site en cliquant sur les liens et en interagissant avec les éléments de l'interface.

3. Pour résoudre une énigme, analysez attentivement les informations présentées sur le site et utilisez

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
